var React require('react');
var ga = require('react-google-analytics');
var GAInitiailizer = ga.Initializer;

module.exports = React.createComponent({

	componentDidMount: function() {
		ga('create', this.props.gaUa || 'UA-XXXX-Y', 'auto');
		ga('send', 'pageview');
	},

	render: function() {

		return (
			<html className="no-js" lang="fr">
				<head>
			        <meta charSet="utf-8">
			        <meta httpEquiv="X-UA-Compatible" content="IE=edge">
			        <title>{{ this.props.title }}</title>
			        <meta name="description" content="">
			        <meta name="viewport" content="width=device-width, initial-scale=1">

			        <link rel="apple-touch-icon" href="apple-touch-icon.png">

			        <link rel="stylesheet" href="css/normalize.css">
			        <link rel="stylesheet" href="css/main.css">
			    </head>

        		<body>
    	    		//if lt IE 8
	        		{{ document.all && !document.addEventListener ? (<p className="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>) : '' }}

          			<div id='container' role="main" dangerouslySetInnerHTML={{__html: this.props.markup}}></div>
          			<script src={this.props.jsbuild}></script>
          			<GAInitiailizer />
    			</body>
      		</html>
  		);
	}
});